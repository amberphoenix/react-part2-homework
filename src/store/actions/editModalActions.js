import { HIDE_EDIT_MODAL, SHOW_EDIT_MODAL } from "../actionTypes";

export const showEditModal = () => ({
  type: SHOW_EDIT_MODAL,
});

export const hideEditModal = () => ({
  type: HIDE_EDIT_MODAL,
});
