import {
  CREATE_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
  SET_MESSAGES_ERROR,
  SET_MESSAGES_SUCCESS,
  SET_LOADER,
  EDIT_MESSAGE,
} from "../actionTypes";

export const setLoader = (isLoading) => ({
  type: SET_LOADER,
  payload: {
    isLoading,
  },
});

export const setMessagesSuccess = (messages) => ({
  type: SET_MESSAGES_SUCCESS,
  payload: {
    messages,
  },
});

export const setMessagesError = (error) => ({
  type: SET_MESSAGES_ERROR,
  payload: {
    error,
  },
});

export const createMessage = (message) => ({
  type: CREATE_MESSAGE,
  payload: {
    message,
  },
});

export const deleteMessage = (messageId) => ({
  type: DELETE_MESSAGE,
  payload: {
    messageId,
  },
});

export const likeMessage = (messageId) => ({
  type: LIKE_MESSAGE,
  payload: {
    messageId,
  },
});

export const editMessage = (messageId, messageText) => ({
  type: EDIT_MESSAGE,
  payload: {
    messageId,
    messageText,
  },
});
