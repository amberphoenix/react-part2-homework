import {
  createUsersMap,
  sortMessagesByDate,
  getCurrentUserId,
} from "../../helpers/chatHelpers";
import {
  CREATE_MESSAGE,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
  LIKE_MESSAGE,
  SET_LOADER,
  SET_MESSAGES_ERROR,
  SET_MESSAGES_SUCCESS,
} from "../actionTypes";

const initialState = {
  isLoading: false,
  messages: [],
  messagesAmount: 0,
  activeUserId: "",
  participants: {},
  participantsCount: 0,
  lastMessageTime: "00:00",
  error: "",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_LOADER: {
      const { isLoading } = action.payload;

      return {
        ...state,
        isLoading,
      };
    }

    case SET_MESSAGES_SUCCESS: {
      const { messages } = action.payload;

      sortMessagesByDate(messages);
      messages.forEach((m) => (m.liked = []));
      const messagesAmount = messages.length;

      const lastMessageDate = messages[messagesAmount - 1].createdAt;
      const lastMessageTime = `${new Date(
        lastMessageDate
      ).getHours()}:${new Date(lastMessageDate).getMinutes()}`;

      const participants = createUsersMap(messages);
      const participantsCount = participants.size;

      const activeUserId = getCurrentUserId(messages);

      return {
        ...state,
        messages,
        messagesAmount,
        activeUserId,
        participants,
        participantsCount,
        lastMessageTime,
      };
    }

    case SET_MESSAGES_ERROR: {
      const { error } = action.payload;
      return {
        ...state,
        error,
      };
    }

    case CREATE_MESSAGE: {
      const { message } = action.payload;

      const lastMessageTime = `${new Date(
        message.createdAt
      ).getHours()}:${new Date(message.createdAt).getMinutes()}`;
      const messagesAmount = state.messagesAmount + 1;

      return {
        ...state,
        messages: [...state.messages, message],
        messagesAmount,
        lastMessageTime,
      };
    }

    case DELETE_MESSAGE: {
      const { messageId } = action.payload;

      const filteredMessages = state.messages.filter(
        (message) => message.id !== messageId
      );
      const messagesAmount = filteredMessages.length;
      const lastMessageDate = filteredMessages[messagesAmount - 1].createdAt;
      const lastMessageTime = `${new Date(
        lastMessageDate
      ).getHours()}:${new Date(lastMessageDate).getMinutes()}`;

      return {
        ...state,
        messages: filteredMessages,
        messagesAmount,
        lastMessageTime,
      };
    }

    case LIKE_MESSAGE: {
      const { messageId } = action.payload;

      const currMessage = state.messages.find(({ id }) => id === messageId);
      if (currMessage.liked.includes(state.activeUserId)) {
        currMessage.liked = currMessage.liked.filter(
          (id) => id !== state.activeUserId
        );
      } else {
        currMessage.liked.push(state.activeUserId);
      }

      return {
        ...state,
        messages: [...state.messages],
      };
    }

    case EDIT_MESSAGE: {
      const { messageId, messageText } = action.payload;

      const currMessage = state.messages.find(({ id }) => id === messageId);
      currMessage.text = messageText;
      currMessage.editedAt = new Date();

      return {
        ...state,
        messages: [...state.messages],
      };
    }

    default:
      return state;
  }
}
