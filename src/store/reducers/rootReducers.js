import { combineReducers } from "redux";
import chatReducer from "./chatReducer";
import editModalReducer from "./editModalReducer";

const rootReducer = combineReducers({
  chat: chatReducer,
  editModal: editModalReducer,
});

export default rootReducer;
