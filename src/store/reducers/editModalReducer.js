import { HIDE_EDIT_MODAL, SHOW_EDIT_MODAL } from "../actionTypes";

const initialState = {
  isShown: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SHOW_EDIT_MODAL: {
      return {
        ...state,
        isShown: true,
      };
    }
    case HIDE_EDIT_MODAL: {
      return {
        ...state,
        isShown: false,
      };
    }
    default:
      return state;
  }
}
