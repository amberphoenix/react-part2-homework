import React, { useState, useEffect } from "react";
import ReactModal from "react-modal";
import { connect } from "react-redux";
import {
  setMessagesSuccess,
  setMessagesError,
  createMessage,
  deleteMessage,
  likeMessage,
  editMessage,
  setLoader,
} from "../store/actions/chatActions";
import {
  showEditModal,
  hideEditModal,
} from "../store/actions/editModalActions";
import Loader from "../components/Loader/Loader";
import ChatHeader from "../components/ChatHeader/ChatHeader";
import MessageInput from "../components/MessageInput/MessageInput";
import MessageList from "../components/MessageList/MessageList";
import axios from "axios";
import { v1 as uuid } from "uuid";
import "./Chat.css";

const Chat = (props) => {
  const [editMessage, setEditMessage] = useState({ id: "", text: "" });

  const onSendMessage = (input) => {
    const message = {
      id: uuid(),
      userId: props.activeUserId,
      avatar: props.participants.get(props.activeUserId).avatar,
      user: props.participants.get(props.activeUserId).user,
      text: input,
      createdAt: new Date(),
      editedAt: "",
      liked: [],
    };
    props.createMessage(message);
  };

  const onDeleteMessage = (messageId) => {
    props.deleteMessage(messageId);
  };

  const onLikeMessage = (messageId) => {
    props.likeMessage(messageId);
  };

  const onEditMessage = (messageId) => {
    const currMessage = props.chatMessages.find(({ id }) => id === messageId);
    setEditMessage(currMessage);
    props.showEditModal();
  };

  const onChangeEditModalTextArea = (event) => {
    setEditMessage({ id: editMessage.id, text: event.target.value });
  };

  const onSaveEditModal = () => {
    props.editMessage(editMessage.id, editMessage.text);
    setEditMessage({ id: "", text: "" });
    props.hideEditModal();
  };

  const onCloseModal = () => {
    props.hideEditModal();
  };

  const onKeyArrowUp = (event) => {
    if (event.key !== "ArrowUp") return;
    const activeUserMessages = props.chatMessages.filter(
      (message) => message.userId === props.activeUserId
    );
    if (!activeUserMessages.length) return;
    const lastMessage = activeUserMessages[activeUserMessages.length - 1];
    setEditMessage(lastMessage);
    props.showEditModal();
  };

  useEffect(() => {
    props.setLoader(true);
    axios
      .get("https://edikdolynskyi.github.io/react_sources/messages.json")
      .then(({ data: messages }) => {
        props.setLoader(false);
        props.setMessagesSuccess(messages);
      })
      .catch((error) => {
        props.setLoader(false);
        props.setMessagesError(error.message);
      });
  }, []);

  if (props.isLoading) return <Loader />;
  if (props.error) return <h1 className="display-center">{props.error}</h1>;

  return (
    <>
      <ChatHeader
        title={"MyChat"}
        participantsNumber={props.participantsCount}
        messagesNumber={props.chatMessagesAmount}
        timeOfLastMessage={props.lastMessageTime}
      />
      <MessageList
        onKeyUp={onKeyArrowUp}
        tabIndex={0}
        activeUser={props.activeUserId}
        messages={props.chatMessages}
        onDelete={onDeleteMessage}
        onLike={onLikeMessage}
        onEdit={onEditMessage}
      />
      <MessageInput onSendButtonClicked={onSendMessage} />
      <ReactModal
        isOpen={props.isShown}
        onRequestClose={onCloseModal}
        className="edit-modal"
        ariaHideApp={false}
      >
        <p className="title">Edit Message</p>
        <textarea
          rows="25"
          cols="60"
          name="edit-modal-input"
          onChange={onChangeEditModalTextArea}
          value={editMessage.text}
        />
        <div className="buttons">
          <button className="button-modal save" onClick={onSaveEditModal}>
            SAVE
          </button>
          <button className="button-modal close" onClick={onCloseModal}>
            CANCEL
          </button>
        </div>
      </ReactModal>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    isLoading: state.chat.isLoading,
    chatMessages: state.chat.messages,
    chatMessagesAmount: state.chat.messagesAmount,
    activeUserId: state.chat.activeUserId,
    participants: state.chat.participants,
    participantsCount: state.chat.participantsCount,
    lastMessageTime: state.chat.lastMessageTime,
    error: state.chat.error,
    isShown: state.editModal.isShown,
  };
};

const mapDispatchToProps = {
  setMessagesSuccess,
  setMessagesError,
  createMessage,
  deleteMessage,
  likeMessage,
  editMessage,
  setLoader,
  showEditModal,
  hideEditModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
