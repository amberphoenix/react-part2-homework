import React from "react";
import "./Layout.css";

const Layout = (props) => {
  return (
    <>
      <header className="header">
        <i className="fas fa-paw logo"></i>
        Chatto
      </header>
      <main className="main">{props.children}</main>
      <footer className="footer">&copy; Copyright</footer>
    </>
  );
};

export default Layout;
