import React from "react";
import "./DayDelimiter.css";

const DayDelimiter = ({ date }) => {
  return <div className="day-delimiter">{date}</div>;
};

export default DayDelimiter;
