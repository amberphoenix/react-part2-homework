import React, { useState } from "react";
import "./MessageInput.css";

const MessageInput = ({ onSendButtonClicked }) => {
  const [inputValue, setInputValue] = useState("");

  const onInputChange = (event) => {
    setInputValue(event.target.value);
  };

  const onButtonClick = () => {
    if (!inputValue.trim()) return;
    onSendButtonClicked(inputValue);
    setInputValue("");
  };

  return (
    <div className="send-area">
      <input
        className="input"
        placeholder="Type your message here.."
        onChange={onInputChange}
        value={inputValue}
      />
      <button className="button" onClick={onButtonClick}>
        SEND
      </button>
    </div>
  );
};

export default MessageInput;
